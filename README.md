# Nudge Algorithm Test (PHP)

## Challenges

### Consecutive Number Series

Write a function that will return true if a given string contains a set of consecutive numbers
otherwise, return false. It is in ascending order always.

#### Examples

`isConsecutive("123456789")` ➞ `true`\
// Contains a set of consecutive ascending numbers\
// if grouped into 1's : 1, 2, 3, 4, 5, 6, 7, 8, 9

`isConsecutive("121314151617")` ➞ `true`\
// Contains a set of consecutive ascending numbers\
// if grouped into 2's : 12, 13, 14, 15, 16, 17

`isConsecutive("123124125")` ➞ `true`\
// Contains a set of consecutive ascending numbers\
// if grouped into 3's : 123, 124, 125

`isConsecutive("121418")` ➞ `false`\
// Group of 2’s but not in a sequence

#### TODO

1. Create a class containing a method named “isConsecutive”
2. Create a Unit test to check the sanity of the method
a. Pass all the values mentioned in the example
b. Create some other values by yourself for testing purposes

### Next Prime

Given an integer, create a function that returns the next prime. If the number is prime, return the number itself.

#### Examples

`nextPrime(12)` ➞ `13`\
`nextPrime(24)` ➞ `29`\
`nextPrime(11)` ➞ `11`\
// 11 is a prime, so we return the number itself.

#### TODO

1. Create a class “NextPrime”
2. Create a method “NextPrimeFinder” taking a parameter
3. Return INT
4. Create a unit test using example to validate the functionality

## Dependencies

* PHP (7.1+)
* Composer

## Commands

* Install Composer dependencies: `composer install`
* Run test cases: `vendor/bin/phpunit`