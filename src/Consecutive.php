<?php

namespace nudge\algotest;

/**
 * Consecutive number checker.
 */
class Consecutive
{
    /**
     * Check if supplied string contains consecutive numbers.
     *
     * @param [string] $string
     * @return boolean
     */
    public function isConsecutive($string) {
        return $this->findConsecutive($string);
    }

    /**
     * Find consecutive numbers in a string.
     *
     * @param string $string we search consecutives in
     * @param int $fragmentSize the starting size of the consecutive sequences we're looking for
     *      - if $fragmentSize = 3 we look for consecutive sequences in the string which are 3 or characters long
     * @return boolean
     */
    private function findConsecutive($string, $fragmentSize = 1) {
        // explode string to int fragments => array
        $fragments = array_map('intval', str_split($string, $fragmentSize));
        $length = sizeof($fragments);

        // we're over the half of the string and we didn't find consecutives we won't find any
        if($fragmentSize > strlen($string) / 2)
            return false;

        // grab first element
        $starter = $fragments[0];
        $i = 0;
        $checkString = "";
        while($i < $length) {
            // generate the real consecutive sequence based on the first element, ith length of $fragments
            $checkString .= ($starter + $i);
            $i++;
        }

        // if we found consecutive, return true, else next consecutive grouping group
        return ($checkString == $string) ? true : $this->findConsecutive($string, $fragmentSize+1);
    }
}