<?php

use PHPUnit\Framework\TestCase;
use nudge\algotest\Consecutive;

/**
 * Test Consecutive class.
 */
class ConsecutiveTest extends TestCase
{

  /**
   * Test isConsecutive method.
   *
   * @return void
   */
  public function testIsConsecutive()
  {
    $consecutive = new Consecutive;

    $this->assertTrue($consecutive->isConsecutive("123456789"));
    $this->assertTrue($consecutive->isConsecutive("121314151617"));
    $this->assertTrue($consecutive->isConsecutive("123124125"));
    $this->assertFalse($consecutive->isConsecutive("121418"));
    $this->assertFalse($consecutive->isConsecutive("121418313127681231"));
    $this->assertFalse($consecutive->isConsecutive("2123131"));
    $this->assertTrue($consecutive->isConsecutive("12341123421234312344123451234612347"));
    $this->assertFalse($consecutive->isConsecutive("765435678908765432456789087654324567890765435678908765432456789087654324567890765435678908765432456789087654324567890765435678908765432456789087654324567890"));
    $this->assertTrue($consecutive->isConsecutive("123124"));
    $this->assertFalse($consecutive->isConsecutive("123125"));


    unset($consecutive);
  }

}
