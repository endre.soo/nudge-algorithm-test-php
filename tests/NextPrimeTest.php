<?php

use PHPUnit\Framework\TestCase;
use nudge\algotest\NextPrime;

/**
 * Test NextPrime class.
 */
class NextPrimeTest extends TestCase
{

  /**
   * Test nextPrimeFinder method.
   *
   * @return void
   */
  public function testNextPrimeFinder()
  {
    $nextPrime = new NextPrime;

    $this->assertEquals(13, $nextPrime->nextPrimeFinder(12));
    $this->assertEquals(29, $nextPrime->nextPrimeFinder(24));
    $this->assertEquals(11, $nextPrime->nextPrimeFinder(11));
    $this->assertEquals(2, $nextPrime->nextPrimeFinder(2));
    $this->assertEquals(2, $nextPrime->nextPrimeFinder(1));
    $this->assertEquals(10133, $nextPrime->nextPrimeFinder(10121));
    $this->assertEquals(2000029, $nextPrime->nextPrimeFinder(2000029)); // even larger

    unset($nextPrime);
  }

}
