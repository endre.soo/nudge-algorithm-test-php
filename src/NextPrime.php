<?php

namespace nudge\algotest;

/**
 * Next prime number finder.
 */
class NextPrime
{

    /**
     * Return the next prime number for supplied integer.
     *
     * @param [int] $number
     * @return void
     */
    public function nextPrimeFinder($number) {
        return $this->findNextPrime($number);
    }

    /**
     * Determines if the number is prime not
     * @param int the number to check if ti's prime
     * @return bool true if prime, false otherwise
     */
    private function isPrime($number) {
        if ($number == 1)
            return false;


        // 2 is the first prime, then from now on, till the square root of the number
        // if we don't find any int divident till the half of the number we won't find one after that
        $nSquareRoot = sqrt($number);
        for ($i = 2; $i <= $nSquareRoot; $i++){
            if ($number % $i == 0)
                return false;
        }

        return true;
    }
    /**
     * Find the next prime number after the provided one
     * @param int the number to check if ti's prime
     * @return int the next prime number
     */
    private function findNextPrime($number) {
        // if the current number is prime, return it
        if($this->isPrime($number))
            return $number;

        $next = $number;
        $primeFound = false;

        // increase $number by 1 until we find the next prime
        while (!$primeFound) {
            $next++;

            if ($this->isPrime($next))
                $primeFound = true;
        }

        // return with next prime
        return $next;

    }

}